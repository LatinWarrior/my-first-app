export interface IVehicle {
    id: number;
    name: string;
}

export class Vehicle {
    constructor(public id: number, public name: string) {

    }
}
