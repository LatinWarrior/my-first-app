import { Component, OnInit } from '@angular/core';
import { IVehicle, Vehicle } from '../.././entity/vehicle'

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {

  private vehicles: Array<IVehicle>;

  constructor() { }

  ngOnInit() {
  }

}
